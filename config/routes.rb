Rails.application.routes.draw do
    # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

    # Defines the root path route ("/")
    root "main#index"

    get 'about-us', to: 'about#index', as: :about

    get 'password', to: 'passwords#edit', as: :edit_password
    patch 'password', to: 'passwords#update'

    get 'forgot_password', to: 'password_resets#new'
    post 'forgot_password', to: 'password_resets#create'
    get 'forgot_password/edit', to: 'password_resets#edit'
    patch 'forgot_password/edit', to: 'password_resets#update'

    get 'sign_up', to: 'registrations#new'
    post 'sign_up', to: 'registrations#create'

    get 'sign_in', to: 'sessions#new'
    post 'sign_in', to: 'sessions#create'
    delete 'logout', to: 'sessions#destroy'

end
