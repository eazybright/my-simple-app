class PasswordResetsController < ApplicationController
    def new
    end

    def create
        @user = User.find_by(email: params[:email])
        if @user.present?
            #send email
            PasswordMailer.with(user: @user).reset.deliver_now
        end

        redirect_to root_path, notice: "If you have account with us a password reset link has been sent to your email."
    end

    def edit
        @user = User.find_signed!(params[:token], purpose: 'password reset')
        rescue ActiveSupport::MessageVerifier::InvalidSignature
            redirect_to sign_in_path, alert: "Expired link. Please try again."
    end

    def update
        @user = User.find_signed(params[:token], purpose: 'password reset')
        if @user.update(password_params)
            redirect_to root_path, notice: "Password reset successfully"
        else
            render :edit, status: 422
        end 
    end

    private

    def password_params
        params.require(:user).permit(:password, :password_confirmation)
    end 
end